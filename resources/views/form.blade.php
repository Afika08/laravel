<!DOCTYPE html>
<head>
    <title>Create New Account</title>
</head>
<body>
    <h1><strong>Buat Account Baru!</strong></h1>
    <h3>Sign Up Form</h3>
    <form action="/post" method="POST">
        @csrf
        <!-- First Name -->
        <label>First name:</label><br><br>
        <input type="text" id="FirstName" name="FirstName"> <br><br>
        <!-- Last Name -->
        <label for="LastName">Last name:</label><br><br>
        <input type="text" id="LastName" name="LastName"><br>    
        <!-- Gender  -->
         <p> Gender:</p>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br>       
        <!-- Nationality -->
        <p>Nationality:</p>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="Singapuran">Singapuran</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select>
        <!--Language Spoken -->
        <p>Language Spoken:</p>
        <input type="checkbox" id="language1" name="language1" value="Bahasa Indonesia">
        <label for="language1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="language2" name="language2" value="English">
        <label for="language2">English</label><br>
        <input type="checkbox" id="language3" name="language3" value="Other">
        <label for="language3">Other</label><br>
    
        <p>Bio:</p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br><br> 
        <!-- --> <br>
        <input type="submit" value="Sign Up"> 
</form>
</body>
</html>